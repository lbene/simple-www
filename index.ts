import { serveListener } from "https://deno.land/std@0.201.0/http/server.ts";

const listener = Deno.listen({ port: 8000 });

function handler(req: Request): Response {
  console.log(req)
  return new Response("test", {
    status: 200,
  })
}

serveListener(listener, handler)
